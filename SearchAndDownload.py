#! /usr/bin/python3.5
import glob, os, subprocess
os.chdir("/media/NAS")
for file in glob.iglob("Downloads/**/*.torrent",recursive=True):
    containingDirectoryPath = os.path.realpath(os.path.dirname(file))
    dirPathWithQuotes='"'+containingDirectoryPath+'"'
    fileNameWithQuotes='"'+file+'"'
    subprocess.call(["deluge-console","connect","127.0.0.1:58846",";","add","-p",dirPathWithQuotes,fileNameWithQuotes,";","exit"])
    os.remove(file)